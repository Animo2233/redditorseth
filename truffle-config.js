/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */

// require('babel-register')({
//   ignore: /node_modules\/(?!testUtils.js|zeppelin-solidity)/,
//   presets: [
//     ['env', {
//       targets : {
//         node : '8.0'
//       }
//     }]
//   ],
//   retainLines: true
// });
// require('babel-polyfill');

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*' // matching any id
    },
    kovan: {
      web3 = new Web3(new Web3.providers.HttpProvider('https://kovan.infura.io/YjJOPQ1J3Iw1QPeH3xRS'));
      account = web3.eth.accounts.privateKeyToAccount(process.env.PRIVATE_KEY);
     
    }
  }
};
